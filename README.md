## INTRO

Hey guys, I am a big fan of sniping and overall dex trading bots so I built a very simple version of a sniping bot I personally use. It's opensourced so check it out in the and modify the settings to your liking. woosh

What is done:

[x] Buy early token/coins with custom gas fee, slippage, amount.

[x] Open source, with free node services already connected

To be done:

[ ] Sell bought token (Working on it)

[ ] Make an option to instantly buy the token, if you want to convert sniping bot into a trading bot for example

Code-preview 

## HOW TO RUN

    1.$ npm install (<---- write this after you open the folder in the terminal of your favorite code editor)
    2.Set the settings in "Bot settings" at the top of bot.js
    3.Input enough funds for fees and purchases into your wallet
    4.Run with "node bot.js" command in the same terminal
    5.Stop bot with Ctrl + C.

## TIPS AND TROUBLESHOOTING


    2.For Uniswap sniping have at least 0.5 ETH to get everything working smooth as some tokens you will want to snipe have big slippage and if the transaction fails you still pay the gas so don't waste money
    3.Check new tokens on dextools
    4.DYOR on dextools and see if the token contract you are sniping doesn't have rug pulls included

WARNING This bot is free and I did it as a hobby project. Great starting place for new devs and snipers. DYOR.

## TROUBLESHOOT If your transaction failed:

    1.Your gas price is too small
    2.Your slippage is too small (use 20+ for early token)


---